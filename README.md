# Revealing latent stochastic dynamics from spikes





<div align="center">   
   <img src="intro.png" alt="latent dynamics inference by dimitra maoutsa" width="75%" height="75%"> 
   </div>


 We are developing a framework for identifying latent stochastic dynamics from spike train observations.
 Based on our previous work on stochastic control [[Maoutsa and Opper, 2021](https://nips.cc/Conferences/2021/ScheduleMultitrack?event=37226), [Maoutsa and Opper, 2022](https://doi.org/10.1103/PhysRevResearch.4.043035), [deterministic particle control framework](https://dimitra-maoutsa.gitlab.io/probability-flow-dynamics-for-constrained-stochastic-nonlinear-systems)] and on inference of stochastic nonlinear systems [[Maoutsa, 2022](https://nips.cc/Conferences/2022/ScheduleMultitrack?event=56993), [geometric inference framework](https://dimitra-maoutsa.gitlab.io/inferring-stochastic-systems-from-discrete-time-observations/)]
, we propose a novel approach that reformulates the inference problem of latent stochastic dynamics in terms of stochastic control.

 <div align="center">   
   <img src="Example_state_spikes.png" alt="latent dynamics inference by dimitra maoutsa" width="25%" height="25%"> 
   </div>


 


<div align="center">   
   <img src="Example_state_est.png" alt="latent dynamics inference by dimitra maoutsa" width="55%" height="55%"> 
   </div>


  
